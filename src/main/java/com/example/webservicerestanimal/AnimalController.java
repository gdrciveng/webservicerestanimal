package com.example.webservicerestanimal;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/animals")
public class AnimalController {

    @GetMapping
    public List<Animal> all(){
        return List.of(
                new Animal(UUID.randomUUID().toString(),"Zebra","X","",""),
                new Animal(UUID.randomUUID().toString(),"Elephant","Y","","")
        );
    }

    @PostMapping
    public Animal createAnimal(@RequestBody CreateAnimal createAnimal){
        return new Animal(
                UUID.randomUUID().toString(),
                createAnimal.getName(),
                createAnimal.getBinomialName(),
                "",
                ""
        );
    }

    @GetMapping("/{id}")
    public Animal get(@PathVariable("id") String id){
        return new Animal(
                id,
                "Zebra",
                "x",
                "",
                ""
        );
    }
}
