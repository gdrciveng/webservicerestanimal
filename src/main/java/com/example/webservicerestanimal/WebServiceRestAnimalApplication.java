package com.example.webservicerestanimal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebServiceRestAnimalApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebServiceRestAnimalApplication.class, args);
	}

}
