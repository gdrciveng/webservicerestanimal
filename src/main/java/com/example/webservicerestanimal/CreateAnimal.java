package com.example.webservicerestanimal;

import lombok.Value;

@Value
public class CreateAnimal {
    String id;
    String name;
    String binomialName;
    String description;
    String conservationStatus;
}
